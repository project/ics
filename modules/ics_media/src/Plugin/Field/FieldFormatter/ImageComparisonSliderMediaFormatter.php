<?php

namespace Drupal\ics_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\ics\Plugin\Field\FieldFormatter\ImageComparisonSliderFormatter;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'Image Comparison Slider' formatter for media.
 *
 * @FieldFormatter(
 *   id = "image_comparison_slider_media",
 *   label = @Translation("Image Comparison Slider"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ImageComparisonSliderMediaFormatter extends ImageComparisonSliderFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $entities = parent::getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($entities)) {
      return [];
    }

    $settings = $this->getSettings();

    $style = $this->getImageStyle($settings);

    $cache_tags = is_null($style) ? [] : $style->getCacheTags();

    $options = $this->buildTemplateOptions($items, $cache_tags);

    $images = [];

    foreach ($entities as $media) {
      $source_field_name = $media->getSource()
        ->getConfiguration()['source_field'];
      $image_item = $media->get($source_field_name)[0];
      // Experimental handling of media entities referencing other entities.
      // See: https://www.drupal.org/project/media_entity_reference.
      if (($image_item instanceof EntityReferenceItem)
        && !($image_item instanceof ImageItem)) {
        $target_entity = $image_item->entity;
        // We only handle referenced entities of type Media.
        if ($target_entity instanceof MediaInterface) {
          $target_source_field_name = $target_entity->getSource()
            ->getConfiguration()['source_field'];
          $image_item = $target_entity->get($target_source_field_name)[0];
        }
      }
      // We only handle medias of type Image.
      if ($image_item instanceof ImageItem) {
        $image = $this->buildImageData($image_item, $media, $style);
        if (!is_null($image)) {
          $images[] = $image;
          $cache_tags = Cache::mergeTags(
            $cache_tags, $image_item->entity->getCacheTags());
        }

        if (count($images) == 2) {
          break;
        }
      }
    }

    return $this->buildRenderArray($images, $options, $cache_tags);
  }

  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // media items.
    return ($field_definition->getFieldStorageDefinition()
      ->getSetting('target_type') == 'media');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return $entity->access('view', NULL, TRUE)
      ->andIf(parent::checkAccess($entity));
  }

}
