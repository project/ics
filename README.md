## Image Comparison Slider

This module provides a new formatter for image field
with Before After sliding effect to compare images.

## INSTALLATION

- Download and enable this module.
- Download the **[Image Comparison Slider](https://github.com/sneas/img-comparison-slider)**
  library and extract it to `/libraries` directory,
  correct path for the javascript file should be
  `/libraries/img-comparison-slider/dist/index.js`.<br>
  The **Image Comparison Slider** library can also be automatically installed
  using the `composer.libraries.json` file.<br>
  You can also install it manually by running the following command:
  `composer require npm-asset/img-comparison-slider`
- Select **Image Comparison Slider** formatter in your multivalued
  image field display configuration,
- Remember that you have to upload at least 2 images.
