<?php

namespace Drupal\ics\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\ics\ImageComparisonSliderUtils as ICSUtils;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Image Comparison Slider' formatter.
 *
 * @FieldFormatter(
 *   id = "image_comparison_slider",
 *   label = @Translation("Image Comparison Slider"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageComparisonSliderFormatter extends ImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator, EntityFieldManagerInterface $entity_field_manager, $logger_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->entityFieldManager = $entity_field_manager;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('entity_field.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'starting_position' => '',
      'additional_options' => '',
      'caption_field' => '',
      'options_field' => '',
      'handle' => '',
      'styles' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $elements['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    $elements['starting_position'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting position'),
      '#description' => $this->t('The starting position is expressed as a percentage, ex: 70.  Defaults to 50 if empty.'),
      '#default_value' => $settings['starting_position'],
      '#placeholder' => 'Ex: 30',
    ];

    $elements['additional_options'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Additional options'),
      '#description' => $this->t('Allows to define additional options for the ICS image formatter, ex: direction=vertical to slide between images vertically.'),
      '#default_value' => $settings['additional_options'],
      '#placeholder' => 'Ex: direction=vertical or hover=true',
    ];

    $available_fields_for_options = $this->getEntityFieldsByType(['string_long']);
    $elements['options_field'] = [
      '#title' => $this->t('Options field'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('options_field'),
      '#empty_option' => $this->t('None'),
      '#options' => $available_fields_for_options,
      '#description' => $this->t('Specify the parent entity field that we should use to get entity specific slider configuration options.'),
    ];

    $available_fields_for_caption = $this->getRelatedEntityFieldsByType(['string', 'string_long']);
    $elements['caption_field'] = [
      '#title' => $this->t('Caption field'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('caption_field'),
      '#empty_option' => $this->t('None'),
      '#options' => $available_fields_for_caption,
      '#description' => $this->t('Specify the related entity field that we should use as a caption for slider images.'),
    ];

    $elements['handle'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Custom SVG handle'),
      '#description' => $this->t('Allows to define a custom SVG handle, check examples documentation here:  https://img-comparison-slider.sneas.io/examples.html#custom-handle'),
      '#default_value' => $settings['handle'],
      '#placeholder' => 'Ex: <svg slot="handle" class="custom-handle" xmlns="http://www.w3.org/2000/svg" ...',
    ];

    $elements['styles'] = [
      '#type' => 'textarea',
      '#rows' => 15,
      '#title' => $this->t('Custom CSS'),
      '#description' => $this->t('Allows to make some quick CSS tweaks but should better be done in your site theme.  Examples available here: https://img-comparison-slider.sneas.io/examples.html'),
      '#default_value' => $settings['styles'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    if ($settings['image_style']) {
      $summary[] = $this->t('Image style: @image_style', ['@image_style' => $settings['image_style']]);
    }

    if ($settings['starting_position']) {
      $summary[] = $this->t('Starting position: @starting_position', ['@starting_position' => $settings['starting_position']]);
    }

    if (!empty($settings['options_field'])) {
      $summary[] = $this->t('Options field: @field', ['@field' => $settings['options_field']]);
    }

    if (!empty($settings['caption_field'])) {
      $summary[] = $this->t('Caption field: @field', ['@field' => $settings['caption_field']]);
    }

    if (!empty($settings['handle'])) {
      $summary[] = $this->t('Custom handle: yes');
    }

    if (!empty($settings['styles'])) {
      $summary[] = $this->t('Custom CSS: yes');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $files = parent::getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return [];
    }

    $settings = $this->getSettings();

    $style = $this->getImageStyle($settings);

    $cache_tags = is_null($style) ? [] : $style->getCacheTags();

    $options = $this->buildTemplateOptions($items, $cache_tags);

    $images = [];
    foreach ($files as $file) {
      $item = $file->_referringItem;
      $image = $this->buildImageData($item, NULL, $style);
      if (!is_null($image)) {
        $images[] = $image;
        $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());
      }
      if (count($images) == 2) {
        break;
      }
    }

    return $this->buildRenderArray($images, $options, $cache_tags);
  }

  /**
   * Build options array for rendering template.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   List of image entities to use form rendering slider.
   * @param array $cache_tags
   *   List of cache tags to update if needed, depending on options source.
   *
   * @return array
   *   Options for template.
   */
  protected function buildTemplateOptions(
    FieldItemListInterface $items,
    array &$cache_tags,
  ): array {

    $options = [];

    $settings = $this->getSettings();

    $options['starting_position'] = $settings['starting_position'] ?? '50%';
    $options['handle'] = !empty($settings['handle']) ? Xss::filter($settings['handle'], ['svg', 'path']) : '';
    $options['styles'] = !empty($settings['styles']) ? Xss::filter($settings['styles']) : '';

    $additional_options = ICSUtils::parseKeyValueString($settings['additional_options']);

    if (!empty($settings['options_field'])) {
      $parent_entity = $items->getParent()->getEntity();
      if (isset($parent_entity)
        && $parent_entity->hasField($settings['options_field'])) {
        $field_items = $parent_entity->get($settings['options_field']);
        if (!$field_items->isEmpty()) {
          foreach ($field_items as $field_item) {
            $entity_specific_options = ICSUtils::parseKeyValueString($field_item->value);
            $cache_tags = Cache::mergeTags($cache_tags, $parent_entity->getCacheTags());
            $options = array_merge($options, $entity_specific_options);
          }
        }
      }
    }

    return array_merge($options, $additional_options);
  }

  /**
   * Generate image data for the Twig template from an image item.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item
   *   The image item.
   * @param \Drupal\Core\Entity\ContentEntityInterface $caption_entity
   *   The entity to get the image caption from.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style to use for display.
   *
   * @return array|null
   *   The image data for the Twig template or NULL if file entity missing.
   */
  protected function buildImageData(
    $image_item,
    $caption_entity,
    ?ImageStyleInterface $style,
  ): array|null {
    $image_entity = $image_item->entity;
    if ($image_entity !== NULL) {
      $url = is_null($style)
        ? $this->fileUrlGenerator->generateAbsoluteString($image_entity->getFileUri())
        : $style->buildUrl($image_entity->getFileUri());

      $alt = $image_item->get('alt')->getValue();

      $image = [];
      $image['url'] = $url;
      $image['alt'] = $alt;

      $settings = $this->getSettings();
      if (!empty($settings['caption_field'])) {
        $caption_field = $settings['caption_field'];
        if (is_null($caption_entity)) {
          $this->setCaptionFromImageItem(
            $image_item, $caption_field, $image);
        }
        else {
          if ($caption_entity->hasField($caption_field)) {
            $this->setCaptionFromEntity(
              $caption_entity, $caption_field, $image);
          }
          // Handle image alt and title cases.
          else {
            $this->setCaptionFromImageItem(
              $image_item, $caption_field, $image);
          }
        }
      }

      return $image;
    }
    else {
      return NULL;
    }
  }

  /**
   * Load ImageStyle entity to use for display.
   *
   * @param array $settings
   *   Formatter settings.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The ImageStyle object.
   */
  protected function getImageStyle(array $settings): ?ImageStyleInterface {
    try {
      return $this->imageStyleStorage->load($settings['image_style']);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ics')->warning($e->getMessage());
      return NULL;
    }
  }

  /**
   * Builds the CIS template render array.
   *
   * @param array $images
   *   Images array to add to the ICS slider.
   * @param array $options
   *   Configuration options for the ICS slider.
   * @param array $cache_tags
   *   Array for cache tags.
   *
   * @return array
   *   The Image Comparison Slider render array.
   */
  protected function buildRenderArray(
    array $images,
    array $options,
    array $cache_tags,
  ): array {
    return [
      '#theme' => 'image_comparison_slider',
      '#images' => $images,
      '#options' => $options,
      '#cache' => [
        'tags' => $cache_tags,
      ],
      '#attached' => [
        'library' => [
          'ics/ics',
        ],
      ],
    ];
  }

  /**
   * Build a list of all text fields of the related entity.
   *
   * @return array
   *   The related entity text fields.
   */
  protected function getEntityFieldsByType(array $types): array {

    $found_fields = [];

    try {
      $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
      $bundle = $this->fieldDefinition->getTargetBundle();

      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

      foreach ($fields as $field_name => $field_definition) {
        // Check if the field type is in list of wanted types.
        if (in_array($field_definition->getType(), $types)) {
          $found_fields[$field_name] = $field_definition->getLabel();
        }
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ics')->warning($e->getMessage());
    }

    return $found_fields;
  }

  /**
   * Build a list of all text fields of the related entity.
   *
   * @return array
   *   The related entity text fields.
   */
  protected function getRelatedEntityFieldsByType(array $types): array {

    $found_fields = [];

    $field_type = $this->fieldDefinition->getType();
    $field_settings = $this->fieldDefinition->getSettings();

    if ($field_type === 'image') {
      if ($field_settings['alt_field']) {
        $found_fields['alt'] = $this->t('Image Alt');
      }
      if ($field_settings['title_field']) {
        $found_fields['title'] = $this->t('Image Title');
      }
    }
    else {
      $found_fields['alt'] = $this->t('Image Alt (if available)');
      $found_fields['title'] = $this->t('Image Title (if available)');
    }

    if ($field_type === 'entity_reference') {
      try {
        $handler_settings =
          $this->fieldDefinition->getSetting('handler_settings');

        $entity_type = $field_settings['target_type'];
        $bundles = $handler_settings['target_bundles'];

        foreach ($bundles as $bundle) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
          foreach ($fields as $field_name => $field_definition) {
            // Check if the field type is in list of wanted types.
            if (in_array($field_definition->getType(), $types)) {
              $found_fields[$field_name] =
                $field_definition->getLabel() . ' (' . $field_name . ')';
            }
          }
        }
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('ics')->warning($e->getMessage());
      }
    }

    return $found_fields;
  }

  /**
   * Defines slider image caption from referenced entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the caption from.
   * @param string $caption_field
   *   The caption field name.
   * @param array $image
   *   The image array to set the caption to.
   */
  protected function setCaptionFromEntity(
    ContentEntityInterface $entity,
    string $caption_field,
    array &$image,
  ): void {
    try {
      $caption = $entity->get($caption_field)[0]->value;
      if (!empty($caption)) {
        $image['caption'] = $caption;
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ics')->warning($e->getMessage());
    }
  }

  /**
   * Defines slider image caption from image item.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item
   *   The entity to get the caption from.
   * @param string $caption_field
   *   The caption field name.
   * @param array $image
   *   The image array to set the caption to.
   */
  protected function setCaptionFromImageItem(
    ImageItem $image_item,
    string $caption_field,
    array &$image,
  ): void {
    try {
      $caption = $image_item->get($caption_field)->getValue();
      if (!empty($caption)) {
        $image['caption'] = $caption;
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('ics')->warning($e->getMessage());
    }
  }

}
