<?php

namespace Drupal\ics;

/**
 * Various utility methods.
 */
class ImageComparisonSliderUtils {

  /**
   * Extract "key=value" pairs from multiline text.
   *
   * Function that parses a multiline string where each line contains a
   * key-value pair separated by an equal sign, and returns an associative
   * array of these keys and values.
   *
   * @param string $string
   *   La chaîne à analyser.
   *
   * @return array
   *   Le tableau associatif contenant les clés et les valeurs.
   */
  public static function parseKeyValueString($string): array {
    // Tableau pour stocker les résultats.
    $result = [];
    // Sépare la chaîne en lignes.
    $lines = explode("\n", $string);

    foreach ($lines as $line) {
      // Vérifie si la ligne n'est pas vide.
      if (trim($line) != '') {
        // Sépare la ligne en deux au niveau du signe égal.
        [$key, $value] = explode('=', $line, 2);
        // Nettoie les espaces blancs autour de la clé.
        $key = trim($key);
        // Nettoie les espaces blancs autour de la valeur.
        $value = trim($value);
        if ($key != '') {
          // Ajoute le couple clé-valeur au tableau résultat.
          $result[$key] = $value;
        }
      }
    }

    // Retourne le tableau associatif.
    return $result;
  }

}
