(($, Drupal, once) => {
  Drupal.behaviors.image_comparison_slider = {
    attach(context) {
      once("ics", "img-comparison-slider", context).forEach(() => {
        // Nothing to do.
      });
    }
  };
})(jQuery, Drupal, once);
